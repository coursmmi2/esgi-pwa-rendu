import page from 'page';
import checkConnectivity from './network.js';
import { api_fetchTodos, api_fetchTodo, api_createTodo, api_deleteTodo, api_updateTodo } from './api/todo.js';
import { idb_setTodos, idb_setTodo, idb_getTodos, idb_getTodo, idb_getDeletedTodos, idb_getAll, idb_unsetTodo, idb_updateTodo } from './idb.js';

checkConnectivity({});

const app  = document.querySelector('#app .outlet');
let create_todo,home_delete_todo, todo_delete_todo, update_todo;

let connection_changed = async function (e){
  document.offline = !e.detail;
  if (!document.offline) {
    let needUpdate = false;
    console.log("connection changed online");
    let todos = await idb_getAll();

    let finalTodos =  Promise.all(todos.map(async(todo)=>{
      //  console.log(todo, index);
       // Update unsynced data
       if (todo.synced === "false"){
          needUpdate = true;
         if(todo.deleted === "true"){
          api_deleteTodo(todo.id);
          idb_unsetTodo(todo.id);
          console.log("idb/api todo teleted");
         } else{
           // Update or create non synced todo
           todo.synced = "true";
           idb_updateTodo(todo).then(() => console.log("Idb updated"));
           let result = await api_fetchTodo(todo.id);
           if(Object.keys(result).length > 0){
            api_updateTodo(todo);
            console.log('todo updated');
           } else{
             api_createTodo(todo);
              console.log('todo created');
           }
         }
       }
       return todo;
    }));

    // console.log("needUpdate",needUpdate);
    if(needUpdate){
      console.log("connexion rerender");
      href = window.location.href;
      if(href.indexOf('todos')>=0){
        id = href.split('todos/')[1];
        page.redirect(`/todos/${id}`);
      } else{
        page.redirect('/home');
      }
    }
    // Rerender
  }
}

fetch('/config.json')
  .then((result) => result.json())
  .then(async (config) => {
    console.log('[todo] Config loaded !!!');
    window.config = config;

    page('/', async () => {
      page.redirect('/home');
    });

    page('/home', async () => {
      const module = await import('./views/home.js');
      const Home = module.default;

      const ctn = app.querySelector('[page="Home"]');
      const homeView = new Home(ctn);

      let todos = [];
      if (!document.offline && navigator.onLine) {
        todos = await idb_getTodos();
        // Update database with idb on reload
        todos.forEach((todo)=>{
          api_updateTodo(todo);
        })
      } else {
        todos = await idb_getTodos() || [];
      }

      homeView.todos = todos;
      homeView.renderView();
      displayPage('Home');

      // Create todo
      create_todo = async function ({ detail: todo }){
        await idb_setTodo(todo);
        if (!document.offline && navigator.onLine === true) {
          // If connection is good enought, do thte HTTP call
          const result = await api_createTodo(todo);
          if (result !== false) {
            // If we successfuly get a result from API
            // Get the updated todo list
            const todos  = await idb_getTodos();
            // Rerender the template
            homeView.todos = todos;
            return homeView.renderView();
          }
          
        }
        // In case of an error
        // Update the synced flag of the new todo
        todo.synced = 'false';
        const todos = await idb_setTodo(todo);
        // Rerender the template
        homeView.todos = todos;
        return homeView.renderView();
      }

      home_delete_todo = async function({ detail: todo }) {
        let todos;
        console.log("delete home");
        debugger
        if (!document.offline && navigator.onLine === true) {
          const result = await api_deleteTodo(todo.id);
          if (result !== false) {
            console.log("connected");
            todos = await idb_unsetTodo(todo.id);
            // homeView.todos = todos;
            // return homeView.renderView();
            page.redirect('/home');
          }
        }
        // In case of an error
        todo.deleted = 'true';
        todo.synced = 'false';
        todos = await idb_updateTodo(todo);
        console.log('todos', todos);

        // homeView.todos = todos;
        // return homeView.renderView();
        page.redirect('/home');

      }

      document.addEventListener('create-todo', create_todo);
      document.addEventListener('delete-todo', home_delete_todo);
      document.addEventListener("connection-changed", connection_changed);

    });

    page(`/todos/:id`, async (ctx)=>{
      console.log("TODOS");
      // console.log(id);
      const id = ctx.params.id;
      console.log(id);
      const module = await import('./views/TodoView.js');
      const TodoView = module.default;

      const ctn = app.querySelector('[page="TodoView"]');
      const todoView = new TodoView(ctn);

      let todo = await idb_getTodo(id);

      todoView.todo = todo;
      todoView.renderView();
      displayPage('TodoView');

      todo_delete_todo = async function ({ detail: todo }) { 
        console.log("delete id");
        debugger

        if (!document.offline && navigator.onLine === true) {
          const response = await api_deleteTodo(todo.id);
          if(response !== false){
            console.log("here");
            await idb_unsetTodo(todo.id);
            page.redirect('/home');
          }
        }
        todo.synced = "false";
        todo.deleted = "true";
        await idb_updateTodo(todo);
          
        page.redirect('/home');
      }
    
      update_todo = async function ({ detail: todo }) {
        if (!document.offline && navigator.onLine === true) {
          const response = await api_updateTodo(todo);
          if(response !== false){
            await idb_updateTodo(todo);
            console.log("todo", todo)
            // todoView.todo = todo;
            // return todoView.renderView();
            return page.redirect(`/todos/${todo.id}`);
          }
        }
        console.log("no connexion");
        todo.synced = "false";
        await idb_updateTodo(todo);
          
        // todoView.todo = todo;
        // return todoView.renderView();
        page.redirect(`/todos/${todo.id}`);
      }

      document.addEventListener('update-todo', update_todo);
      document.addEventListener('delete-todo', todo_delete_todo);
      document.addEventListener("connection-changed", connection_changed);

    });

    page();
  });

  
  
  function displayPage(page) {
    console.log("displayPage");
    document.removeEventListener('update-todo', update_todo);
    document.removeEventListener('delete-todo', todo_delete_todo);
    document.removeEventListener('delete-todo', home_delete_todo);
    document.removeEventListener("connection-changed", connection_changed);

    const skeleton = document.querySelector('#app .skeleton');
    skeleton.removeAttribute('hidden');
    const pages = app.querySelectorAll('[page]');
    pages.forEach(page => page.removeAttribute('active'));
    skeleton.setAttribute('hidden', 'true');
    const p = app.querySelector(`[page="${page}"]`);
    p.setAttribute('active', true);
  }