import { render, html } from 'lit-html';
import '../component/todo-card-edit.js';
import 'lit-icon';

export default class TodoView{
    constructor(page) {
      this.page = page;
      this.properties = {
        todo: {
          title : '',
          description : ''
        },
      };

      this.renderView();
    }

    set todo(value) {
      this.properties.todo = value;
    }
  
    get todo() {
      return this.properties.todo;
    }

    template() {
      return html`
        <section class="h-20">
          <div>
            <header>
              <h1 class="mt-2 px-4 text-xl">My awesome todos : </h1>
            </header>
            <main class="todolist px-4 pb-20">
              <ul>
                <todo-card-edit .todo="${this.properties.todo}"></todo-card-edit>
              </ul>
            </main>
          </div>
        </section>
        <lit-iconset>
          <svg><defs>
            <g id="delete"><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"></path></g>
            <g id="cloud-off"><path d="M19.35 10.04C18.67 6.59 15.64 4 12 4c-1.48 0-2.85.43-4.01 1.17l1.46 1.46C10.21 6.23 11.08 6 12 6c3.04 0 5.5 2.46 5.5 5.5v.5H19c1.66 0 3 1.34 3 3 0 1.13-.64 2.11-1.56 2.62l1.45 1.45C23.16 18.16 24 16.68 24 15c0-2.64-2.05-4.78-4.65-4.96zM3 5.27l2.75 2.74C2.56 8.15 0 10.77 0 14c0 3.31 2.69 6 6 6h11.73l2 2L21 20.73 4.27 4 3 5.27zM7.73 10l8 8H6c-2.21 0-4-1.79-4-4s1.79-4 4-4h1.73z"></path></g>
            <g id="send"><path d="M2.01 21L23 12 2.01 3 2 10l15 2-15 2z"></path></g>
            <g id="edit">
              <svg xmlns="http://www.w3.org/2000/svg"   
              viewBox="0 0 24 24" fill="grey" stroke="currentColor" 
              stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit">
  
              <path d="M20 14.66V20a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h5.34"></path>
  
              <polygon points="18 2 22 6 12 16 8 16 8 12 18 2"></polygon>
  
              </svg>
            </g>
            <g id="validate">
              <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
              <path style="fill:#A5EB78;" d="M433.139,67.108L201.294,298.953c-6.249,6.249-16.381,6.249-22.63,0L78.861,199.15L0,278.011
                l150.547,150.549c10.458,10.458,24.642,16.333,39.431,16.333l0,0c14.788,0,28.973-5.876,39.43-16.333L512,145.968L433.139,67.108z"
                />
              <g style="opacity:0.1;">
                <path d="M485.921,119.888L187.59,418.22c-8.254,8.253-18.633,13.882-29.847,16.391c9.363,6.635,20.608,10.28,32.235,10.28l0,0
                  c14.788,0,28.973-5.876,39.43-16.333L512,145.966L485.921,119.888z"/>
              </g>
            </g>
            <g id="cancel">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"/></svg>
            </g>
          </defs></svg>
        </lit-iconset>
      `;
    }
    
  renderView() {
    const view = this.template();
    render(view, this.page);
  }

}