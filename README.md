# Dependecies 
    - NodeJS
    - NPM
    - https://github.com/jakearchibald/idb

## Install dependencies

```
npm install
```

# Run project
```
npm run database
npm run serve
npm run css
```